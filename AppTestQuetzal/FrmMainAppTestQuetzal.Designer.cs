﻿namespace AppTestQuetzal
{
    partial class FrmMainAppTestQuetzal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvPersonAddress = new System.Windows.Forms.DataGridView();
            this.PersonId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MotherLastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddressId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Street = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OutdoorNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InteriorNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Colony = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Municipality = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPersonAddress)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvPersonAddress
            // 
            this.dgvPersonAddress.AllowUserToAddRows = false;
            this.dgvPersonAddress.AllowUserToDeleteRows = false;
            this.dgvPersonAddress.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPersonAddress.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPersonAddress.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PersonId,
            this.FirstName,
            this.LastName,
            this.MotherLastName,
            this.AddressId,
            this.Street,
            this.OutdoorNumber,
            this.InteriorNumber,
            this.Colony,
            this.Municipality});
            this.dgvPersonAddress.Location = new System.Drawing.Point(19, 91);
            this.dgvPersonAddress.Name = "dgvPersonAddress";
            this.dgvPersonAddress.ReadOnly = true;
            this.dgvPersonAddress.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPersonAddress.Size = new System.Drawing.Size(1098, 284);
            this.dgvPersonAddress.TabIndex = 0;
            this.dgvPersonAddress.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPersonAddress_CellContentDoubleClick);
            // 
            // PersonId
            // 
            this.PersonId.HeaderText = "PersonId";
            this.PersonId.Name = "PersonId";
            this.PersonId.ReadOnly = true;
            this.PersonId.Visible = false;
            // 
            // FirstName
            // 
            this.FirstName.HeaderText = "Nombre";
            this.FirstName.MaxInputLength = 150;
            this.FirstName.Name = "FirstName";
            this.FirstName.ReadOnly = true;
            // 
            // LastName
            // 
            this.LastName.HeaderText = "Paterno";
            this.LastName.MaxInputLength = 150;
            this.LastName.Name = "LastName";
            this.LastName.ReadOnly = true;
            // 
            // MotherLastName
            // 
            this.MotherLastName.HeaderText = "Materno";
            this.MotherLastName.MaxInputLength = 150;
            this.MotherLastName.Name = "MotherLastName";
            this.MotherLastName.ReadOnly = true;
            // 
            // AddressId
            // 
            this.AddressId.HeaderText = "AddressId";
            this.AddressId.Name = "AddressId";
            this.AddressId.ReadOnly = true;
            this.AddressId.Visible = false;
            // 
            // Street
            // 
            this.Street.HeaderText = "Calle";
            this.Street.MaxInputLength = 150;
            this.Street.Name = "Street";
            this.Street.ReadOnly = true;
            // 
            // OutdoorNumber
            // 
            this.OutdoorNumber.HeaderText = "Num Exterior";
            this.OutdoorNumber.MaxInputLength = 5;
            this.OutdoorNumber.Name = "OutdoorNumber";
            this.OutdoorNumber.ReadOnly = true;
            // 
            // InteriorNumber
            // 
            this.InteriorNumber.HeaderText = "Num Interior";
            this.InteriorNumber.MaxInputLength = 5;
            this.InteriorNumber.Name = "InteriorNumber";
            this.InteriorNumber.ReadOnly = true;
            // 
            // Colony
            // 
            this.Colony.HeaderText = "Colonia";
            this.Colony.MaxInputLength = 150;
            this.Colony.Name = "Colony";
            this.Colony.ReadOnly = true;
            // 
            // Municipality
            // 
            this.Municipality.HeaderText = "Municipio";
            this.Municipality.MaxInputLength = 150;
            this.Municipality.Name = "Municipality";
            this.Municipality.ReadOnly = true;
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(19, 40);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(75, 23);
            this.btnNew.TabIndex = 1;
            this.btnNew.Text = "Nuevo";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Enabled = false;
            this.btnDelete.Location = new System.Drawing.Point(230, 40);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "Eliminar";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Enabled = false;
            this.btnUpdate.Location = new System.Drawing.Point(128, 40);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 3;
            this.btnUpdate.Text = "Actualizar";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // FrmMainAppTestQuetzal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1139, 388);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.dgvPersonAddress);
            this.Name = "FrmMainAppTestQuetzal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AppTestQuetzal";
            this.Load += new System.EventHandler(this.FrmMainAppTestQuetzal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPersonAddress)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvPersonAddress;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.DataGridViewTextBoxColumn PersonId;
        private System.Windows.Forms.DataGridViewTextBoxColumn FirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn MotherLastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddressId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Street;
        private System.Windows.Forms.DataGridViewTextBoxColumn OutdoorNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn InteriorNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn Colony;
        private System.Windows.Forms.DataGridViewTextBoxColumn Municipality;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnUpdate;
    }
}

