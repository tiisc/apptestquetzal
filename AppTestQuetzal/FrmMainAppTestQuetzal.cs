﻿namespace AppTestQuetzal
{
    using AppTestQuetzalCRUD.PersonAddressCrud;
    using AppTestQuetzalDomain.Models.PersonAddressModel;
    using AppTestQuetzalDomain.Models.PersonModel;
    using System;
    using System.Windows.Forms;

    public partial class FrmMainAppTestQuetzal : Form
    {
        FrmNewItem frmNewItem;
        Person person;
        ListInfoPersonsAddress listInfoPersonsAddress;
        PersonAddressCrud personAddressCrud;
        int? interiorNumber = null;
        int? personId;

        public FrmMainAppTestQuetzal()
        {
            InitializeComponent();
        }

        private void FrmMainAppTestQuetzal_Load(object sender, EventArgs e)
        {
            BindingDgvPersonAddress();
        }

        private void BindingDgvPersonAddress()
        {
            DisabledBtns();
            personId = null;

            if (person != null)
                person = null;

            personAddressCrud = new PersonAddressCrud();
            listInfoPersonsAddress = new ListInfoPersonsAddress();
            listInfoPersonsAddress = personAddressCrud.GetAllPersonsAddress();

            dgvPersonAddress.AutoGenerateColumns = false;
            dgvPersonAddress.DataSource = listInfoPersonsAddress.PersonsAddress;
            dgvPersonAddress.Columns["PersonId"].DataPropertyName = "PersonId";
            dgvPersonAddress.Columns["FirstName"].DataPropertyName = "FirstName";
            dgvPersonAddress.Columns["LastName"].DataPropertyName = "LastName";
            dgvPersonAddress.Columns["MotherLastName"].DataPropertyName = "MotherLastName";
            dgvPersonAddress.Columns["AddressId"].DataPropertyName = "AddressId";
            dgvPersonAddress.Columns["Street"].DataPropertyName = "Street";
            dgvPersonAddress.Columns["OutdoorNumber"].DataPropertyName = "OutdoorNumber";
            dgvPersonAddress.Columns["InteriorNumber"].DataPropertyName = "InteriorNumber";
            dgvPersonAddress.Columns["Colony"].DataPropertyName = "Colony";
            dgvPersonAddress.Columns["Municipality"].DataPropertyName = "Municipality";

            DataGridViewComboBoxColumn dgvComboBoxStateRepublic = new DataGridViewComboBoxColumn
            {
                DataPropertyName = "StateRepublicId",
                Name = "StateRepublicId",
                HeaderText = "Entidad",
                DataSource = listInfoPersonsAddress.StatesRepublic,
                DisplayMember = "NameStateRepublic",
                ValueMember = "StateRepublicId"
            };

            if (dgvPersonAddress.Columns["StateRepublicId"] == null)
            {
                dgvPersonAddress.Columns.Add(dgvComboBoxStateRepublic);            
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            frmNewItem = new FrmNewItem(listInfoPersonsAddress.StatesRepublic);
            frmNewItem.ShowDialog();            
            BindingDgvPersonAddress();
        }        

        private void dgvPersonAddress_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            EnabledBtns();
            interiorNumber = null;
            personId = null;
            
            try
            {
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = dgvPersonAddress.Rows[e.RowIndex];

                    var valueInteriorNumber = row.Cells["InteriorNumber"].Value;

                    if (valueInteriorNumber != null)
                    {
                        int.TryParse(valueInteriorNumber.ToString(), out int tempInteriorNumber);
                        interiorNumber = tempInteriorNumber;
                    }

                    personId = Convert.ToInt32(row.Cells["PersonId"].Value.ToString());

                    person = new Person
                    {
                        PersonId = (int)personId,
                        FirstName = row.Cells["FirstName"].Value.ToString(),
                        LastName = row.Cells["LastName"].Value.ToString(),
                        MotherLastName = row.Cells["MotherLastName"].Value.ToString(),
                        AddressId = Convert.ToInt32(row.Cells["AddressId"].Value.ToString()),
                        Street = row.Cells["Street"].Value.ToString(),
                        OutdoorNumber = Convert.ToInt32(row.Cells["OutdoorNumber"].Value.ToString()),
                        InteriorNumber = interiorNumber,
                        Colony = row.Cells["Colony"].Value.ToString(),
                        Municipality = row.Cells["Municipality"].Value.ToString(),
                        StateRepublicId = Convert.ToInt32(row.Cells["StateRepublicId"].Value.ToString()),
                    };
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void EnabledBtns()
        {
            btnUpdate.Enabled = true;
            btnDelete.Enabled = true;
        }

        private void DisabledBtns()
        {
            btnUpdate.Enabled = false;
            btnDelete.Enabled = false;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            bool isDeleted;
            
            DialogResult dialogResult = MessageBox.Show("¿Está seguro que quiere eliminar el registro?", "Quetzalcóatl Sistemas", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                personAddressCrud = new PersonAddressCrud();
                isDeleted = personAddressCrud.DeletePersonAddress((int)personId);

                if (isDeleted)
                {
                    MessageBox.Show("El registro se eliminó correctamente", "Quetzalcóatl Sistemas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    BindingDgvPersonAddress();
                }
                else
                {
                    MessageBox.Show("Ocurrió un error al eliminar el registro", "Quetzalcóatl Sistemas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (person != null)
            {
                frmNewItem = new FrmNewItem(listInfoPersonsAddress.StatesRepublic, person);
                frmNewItem.ShowDialog();
                BindingDgvPersonAddress();
            }
        }
    }
}