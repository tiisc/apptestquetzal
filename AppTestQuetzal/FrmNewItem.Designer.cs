﻿namespace AppTestQuetzal
{
    partial class FrmNewItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFirstName = new System.Windows.Forms.Label();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.lblLastName = new System.Windows.Forms.Label();
            this.txtMotherLastName = new System.Windows.Forms.TextBox();
            this.lblMotherLastName = new System.Windows.Forms.Label();
            this.txtStreet = new System.Windows.Forms.TextBox();
            this.lblStreet = new System.Windows.Forms.Label();
            this.lblOutdoorNumber = new System.Windows.Forms.Label();
            this.lblInteriorNumber = new System.Windows.Forms.Label();
            this.txtColony = new System.Windows.Forms.TextBox();
            this.lblColony = new System.Windows.Forms.Label();
            this.cbStateRepublic = new System.Windows.Forms.ComboBox();
            this.lblStateRepublic = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtMunicipality = new System.Windows.Forms.TextBox();
            this.lblMunicipality = new System.Windows.Forms.Label();
            this.mtxtOutdoorNumber = new System.Windows.Forms.MaskedTextBox();
            this.mtxtInteriorNumber = new System.Windows.Forms.MaskedTextBox();
            this.SuspendLayout();
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Location = new System.Drawing.Point(79, 27);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(47, 13);
            this.lblFirstName.TabIndex = 0;
            this.lblFirstName.Text = "Nombre:";
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(135, 24);
            this.txtFirstName.MaxLength = 150;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(121, 20);
            this.txtFirstName.TabIndex = 1;
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(135, 60);
            this.txtLastName.MaxLength = 150;
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(121, 20);
            this.txtLastName.TabIndex = 2;
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.Location = new System.Drawing.Point(79, 63);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(47, 13);
            this.lblLastName.TabIndex = 2;
            this.lblLastName.Text = "Paterno:";
            // 
            // txtMotherLastName
            // 
            this.txtMotherLastName.Location = new System.Drawing.Point(135, 96);
            this.txtMotherLastName.MaxLength = 150;
            this.txtMotherLastName.Name = "txtMotherLastName";
            this.txtMotherLastName.Size = new System.Drawing.Size(121, 20);
            this.txtMotherLastName.TabIndex = 3;
            // 
            // lblMotherLastName
            // 
            this.lblMotherLastName.AutoSize = true;
            this.lblMotherLastName.Location = new System.Drawing.Point(79, 99);
            this.lblMotherLastName.Name = "lblMotherLastName";
            this.lblMotherLastName.Size = new System.Drawing.Size(49, 13);
            this.lblMotherLastName.TabIndex = 4;
            this.lblMotherLastName.Text = "Materno:";
            // 
            // txtStreet
            // 
            this.txtStreet.Location = new System.Drawing.Point(135, 131);
            this.txtStreet.MaxLength = 150;
            this.txtStreet.Name = "txtStreet";
            this.txtStreet.Size = new System.Drawing.Size(121, 20);
            this.txtStreet.TabIndex = 4;
            // 
            // lblStreet
            // 
            this.lblStreet.AutoSize = true;
            this.lblStreet.Location = new System.Drawing.Point(79, 134);
            this.lblStreet.Name = "lblStreet";
            this.lblStreet.Size = new System.Drawing.Size(33, 13);
            this.lblStreet.TabIndex = 6;
            this.lblStreet.Text = "Calle:";
            // 
            // lblOutdoorNumber
            // 
            this.lblOutdoorNumber.AutoSize = true;
            this.lblOutdoorNumber.Location = new System.Drawing.Point(79, 170);
            this.lblOutdoorNumber.Name = "lblOutdoorNumber";
            this.lblOutdoorNumber.Size = new System.Drawing.Size(50, 13);
            this.lblOutdoorNumber.TabIndex = 8;
            this.lblOutdoorNumber.Text = "Num Ext:";
            // 
            // lblInteriorNumber
            // 
            this.lblInteriorNumber.AutoSize = true;
            this.lblInteriorNumber.Location = new System.Drawing.Point(79, 202);
            this.lblInteriorNumber.Name = "lblInteriorNumber";
            this.lblInteriorNumber.Size = new System.Drawing.Size(47, 13);
            this.lblInteriorNumber.TabIndex = 10;
            this.lblInteriorNumber.Text = "Num Int:";
            // 
            // txtColony
            // 
            this.txtColony.Location = new System.Drawing.Point(135, 230);
            this.txtColony.MaxLength = 150;
            this.txtColony.Name = "txtColony";
            this.txtColony.Size = new System.Drawing.Size(121, 20);
            this.txtColony.TabIndex = 7;
            // 
            // lblColony
            // 
            this.lblColony.AutoSize = true;
            this.lblColony.Location = new System.Drawing.Point(80, 233);
            this.lblColony.Name = "lblColony";
            this.lblColony.Size = new System.Drawing.Size(45, 13);
            this.lblColony.TabIndex = 12;
            this.lblColony.Text = "Colonia:";
            // 
            // cbStateRepublic
            // 
            this.cbStateRepublic.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbStateRepublic.FormattingEnabled = true;
            this.cbStateRepublic.ItemHeight = 13;
            this.cbStateRepublic.Location = new System.Drawing.Point(135, 290);
            this.cbStateRepublic.Name = "cbStateRepublic";
            this.cbStateRepublic.Size = new System.Drawing.Size(121, 21);
            this.cbStateRepublic.TabIndex = 9;
            // 
            // lblStateRepublic
            // 
            this.lblStateRepublic.AutoSize = true;
            this.lblStateRepublic.Location = new System.Drawing.Point(79, 294);
            this.lblStateRepublic.Name = "lblStateRepublic";
            this.lblStateRepublic.Size = new System.Drawing.Size(46, 13);
            this.lblStateRepublic.TabIndex = 15;
            this.lblStateRepublic.Text = "Entidad:";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(149, 333);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Guardar";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtMunicipality
            // 
            this.txtMunicipality.Location = new System.Drawing.Point(135, 260);
            this.txtMunicipality.MaxLength = 150;
            this.txtMunicipality.Name = "txtMunicipality";
            this.txtMunicipality.Size = new System.Drawing.Size(121, 20);
            this.txtMunicipality.TabIndex = 8;
            // 
            // lblMunicipality
            // 
            this.lblMunicipality.AutoSize = true;
            this.lblMunicipality.Location = new System.Drawing.Point(79, 263);
            this.lblMunicipality.Name = "lblMunicipality";
            this.lblMunicipality.Size = new System.Drawing.Size(55, 13);
            this.lblMunicipality.TabIndex = 17;
            this.lblMunicipality.Text = "Municipio:";
            // 
            // mtxtOutdoorNumber
            // 
            this.mtxtOutdoorNumber.Location = new System.Drawing.Point(135, 167);
            this.mtxtOutdoorNumber.Mask = "00000";
            this.mtxtOutdoorNumber.Name = "mtxtOutdoorNumber";
            this.mtxtOutdoorNumber.Size = new System.Drawing.Size(121, 20);
            this.mtxtOutdoorNumber.TabIndex = 5;
            this.mtxtOutdoorNumber.ValidatingType = typeof(int);
            // 
            // mtxtInteriorNumber
            // 
            this.mtxtInteriorNumber.Location = new System.Drawing.Point(135, 195);
            this.mtxtInteriorNumber.Mask = "00000";
            this.mtxtInteriorNumber.Name = "mtxtInteriorNumber";
            this.mtxtInteriorNumber.Size = new System.Drawing.Size(121, 20);
            this.mtxtInteriorNumber.TabIndex = 6;
            this.mtxtInteriorNumber.ValidatingType = typeof(int);
            // 
            // FrmNewItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(339, 371);
            this.Controls.Add(this.mtxtInteriorNumber);
            this.Controls.Add(this.mtxtOutdoorNumber);
            this.Controls.Add(this.txtMunicipality);
            this.Controls.Add(this.lblMunicipality);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblStateRepublic);
            this.Controls.Add(this.cbStateRepublic);
            this.Controls.Add(this.txtColony);
            this.Controls.Add(this.lblColony);
            this.Controls.Add(this.lblInteriorNumber);
            this.Controls.Add(this.lblOutdoorNumber);
            this.Controls.Add(this.txtStreet);
            this.Controls.Add(this.lblStreet);
            this.Controls.Add(this.txtMotherLastName);
            this.Controls.Add(this.lblMotherLastName);
            this.Controls.Add(this.txtLastName);
            this.Controls.Add(this.lblLastName);
            this.Controls.Add(this.txtFirstName);
            this.Controls.Add(this.lblFirstName);
            this.Name = "FrmNewItem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New Item";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmNewItem_FormClosing);
            this.Load += new System.EventHandler(this.FrmNewItem_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.TextBox txtMotherLastName;
        private System.Windows.Forms.Label lblMotherLastName;
        private System.Windows.Forms.TextBox txtStreet;
        private System.Windows.Forms.Label lblStreet;
        private System.Windows.Forms.Label lblOutdoorNumber;
        private System.Windows.Forms.Label lblInteriorNumber;
        private System.Windows.Forms.TextBox txtColony;
        private System.Windows.Forms.Label lblColony;
        private System.Windows.Forms.ComboBox cbStateRepublic;
        private System.Windows.Forms.Label lblStateRepublic;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtMunicipality;
        private System.Windows.Forms.Label lblMunicipality;
        private System.Windows.Forms.MaskedTextBox mtxtOutdoorNumber;
        private System.Windows.Forms.MaskedTextBox mtxtInteriorNumber;
    }
}