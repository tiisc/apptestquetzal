﻿namespace AppTestQuetzal
{
    using AppTestQuetzalCRUD.PersonAddressCrud;
    using AppTestQuetzalDomain.Models.AddressModel;
    using AppTestQuetzalDomain.Models.PersonModel;
    using AppTestQuetzalHelpers.Enums;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    public partial class FrmNewItem : Form
    {
        List<StateRepublic> listStatesRepublic;
        PersonAddressCrud personAddressCrud;
        Person person;
        int outdoorNumber;
        int? interiorNumber = null;
        string sOutdoorNumber = string.Empty;
        string sInteriorNumber = string.Empty;
        int actionCrudId;
        int personId;
        int addressId;
        private string FieldsRequired { get; set; }

        public FrmNewItem(List<StateRepublic> listStatesRepublic)
        {
            InitializeComponent();
            this.listStatesRepublic = listStatesRepublic;
            actionCrudId = (int)EnumCrud.Create;
        }

        public FrmNewItem(List<StateRepublic> listStatesRepublic, Person person)
        {
            InitializeComponent();
            this.listStatesRepublic = listStatesRepublic;
            actionCrudId = (int)EnumCrud.Update;
            this.person = person;
        }

        private void FrmNewItem_Load(object sender, EventArgs e)
        {
            BindingComboBoxStatesRepublic();

            if (actionCrudId == (int)EnumCrud.Update)
            {
                BindingModelWithItemsFrm();
            }
        }

        private void BindingComboBoxStatesRepublic()
        {
            cbStateRepublic.DataSource = listStatesRepublic;            
            cbStateRepublic.DisplayMember = "NameStateRepublic";
            cbStateRepublic.ValueMember = "StateRepublicId";
        }

        private void BindingModelWithItemsFrm()
        {
            personId = person.PersonId;
            txtFirstName.Text = person.FirstName;
            txtLastName.Text = person.LastName;
            txtMotherLastName.Text = person.MotherLastName;

            addressId = person.AddressId;
            txtStreet.Text = person.Street;
            mtxtOutdoorNumber.Text = person.OutdoorNumber.ToString();

            if (person.InteriorNumber != null)
                mtxtInteriorNumber.Text = person.InteriorNumber.ToString();

            txtColony.Text = person.Colony;
            txtMunicipality.Text = person.Municipality;
            cbStateRepublic.SelectedValue = person.StateRepublicId;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (ValidateFieldsRequired())
            {
                BindingValuesWithModelPersonAndSave();
            }
            else
            {
                string msg = $"Los Los siguientes campos son requeridos:\n {FieldsRequired}";
                MessageBox.Show(msg, "Quetzalcóatl Sistemas", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private bool ValidateFieldsRequired()
        {
            bool isValidAllFields = true;
            FieldsRequired = string.Empty;

            if (string.IsNullOrEmpty(txtFirstName.Text) || string.IsNullOrWhiteSpace(txtFirstName.Text))
            {
                isValidAllFields = false;
                FieldsRequired = "Nombre\n";
            }

            if (string.IsNullOrEmpty(txtLastName.Text) || string.IsNullOrWhiteSpace(txtLastName.Text))
            {
                isValidAllFields = false;
                FieldsRequired += "Paterno\n";
            }

            if (string.IsNullOrEmpty(txtMotherLastName.Text) || string.IsNullOrWhiteSpace(txtMotherLastName.Text))
            {
                isValidAllFields = false;
                FieldsRequired += "Materno\n";
            }

            if (string.IsNullOrEmpty(txtStreet.Text) || string.IsNullOrWhiteSpace(txtStreet.Text))
            {
                isValidAllFields = false;
                FieldsRequired += "Calle\n";
            }

            if (string.IsNullOrEmpty(mtxtOutdoorNumber.Text) || string.IsNullOrWhiteSpace(mtxtOutdoorNumber.Text))
            {
                isValidAllFields = false;
                FieldsRequired += "Num Ext\n";
            }

            if (string.IsNullOrEmpty(txtColony.Text) || string.IsNullOrWhiteSpace(txtColony.Text))
            {
                isValidAllFields = false;
                FieldsRequired += "Colonia\n";
            }

            if (string.IsNullOrEmpty(txtMunicipality.Text) || string.IsNullOrWhiteSpace(txtMunicipality.Text))
            {
                isValidAllFields = false;
                FieldsRequired += "Municipio\n";
            }

            return isValidAllFields;
        }

        private void BindingValuesWithModelPersonAndSave()
        {
            bool savedCorrectly = false;
            string msgActionCrud = string.Empty;
            string msgErrorActionCrud = string.Empty;

            sOutdoorNumber = mtxtOutdoorNumber.Text.Trim();
            int.TryParse(sOutdoorNumber, out outdoorNumber);


            if (!string.IsNullOrEmpty(mtxtInteriorNumber.Text) && !string.IsNullOrWhiteSpace(mtxtInteriorNumber.Text))
            {
                sInteriorNumber = mtxtInteriorNumber.Text.Trim();

                if (int.TryParse(sInteriorNumber, out int tempInteriorNumber))
                    interiorNumber = tempInteriorNumber;
                else
                    interiorNumber = null;                
            }

            person = new Person
            {                            
                FirstName = txtFirstName.Text,
                LastName = txtLastName.Text,
                MotherLastName = txtMotherLastName.Text,
                Street = txtStreet.Text,
                OutdoorNumber = outdoorNumber,
                InteriorNumber = interiorNumber,
                Colony = txtColony.Text,
                Municipality = txtMunicipality.Text,
                StateRepublicId = (int)cbStateRepublic.SelectedValue,
            };
            
            personAddressCrud = new PersonAddressCrud();

            if (actionCrudId == (int)EnumCrud.Create)
            {
                savedCorrectly = personAddressCrud.CreatePersonAddress(person);
                msgActionCrud = "guardó";
                msgErrorActionCrud = "guardar";
            }
            else if (actionCrudId == (int)EnumCrud.Update)
            {
                person.PersonId = personId;
                person.AddressId = addressId;

                savedCorrectly = personAddressCrud.UpdatePersonAddress(person);
                msgActionCrud = "actualizó";
                msgErrorActionCrud = "actualizar";
            }
            
            if (savedCorrectly)
            {
                MessageBox.Show($"La información se {msgActionCrud} correctamente", "Quetzalcóatl Sistemas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            else
            {
                MessageBox.Show($"No se pudo {msgErrorActionCrud} la información, ocurrió un error", "Quetzalcóatl Sistemas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void FrmNewItem_FormClosing(object sender, FormClosingEventArgs e)
        {
            Dispose();
        }
    }
}