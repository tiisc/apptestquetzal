﻿namespace AppTestQuetzalCRUD.PersonAddressCrud
{
    using AppTestQuetzalDataAccess;
    using AppTestQuetzalDomain.Models.AddressModel;
    using AppTestQuetzalDomain.Models.PersonAddressModel;
    using AppTestQuetzalDomain.Models.PersonModel;
    using AppTestQuetzalHelpers.Enums;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;

    public class PersonAddressCrud
    {                
        public bool CreatePersonAddress(Person person)
        {
            DataAccessSqlServer dataAccessSqlServer = new DataAccessSqlServer("AppTestQuetzalConnection");
            List<SqlParameter> sqlParameters = new List<SqlParameter>();
            int intRespondeSP = 0;
            bool boolResponseSP = false;

            try
            {
                DataTable dataTable = new DataTable("tbl_PersonAddressType");

                dataTable.Columns.Add("PersonId", typeof(int));
                dataTable.Columns.Add("FirstName", typeof(string));
                dataTable.Columns.Add("LastName", typeof(string));
                dataTable.Columns.Add("MotherLastName", typeof(string));
                dataTable.Columns.Add("AddressId", typeof(int));
                dataTable.Columns.Add("Street", typeof(string));
                dataTable.Columns.Add("OutdoorNumber", typeof(int));
                dataTable.Columns.Add("InteriorNumber", typeof(int));
                dataTable.Columns.Add("Colony", typeof(string));
                dataTable.Columns.Add("Municipality", typeof(string));
                dataTable.Columns.Add("StateRepublicId", typeof(int));

                DataRow dataRow = dataTable.NewRow();

                dataRow.SetField<int?>("PersonId", person.PersonId);
                dataRow.SetField<string>("FirstName", person.FirstName);
                dataRow.SetField<string>("LastName", person.LastName);
                dataRow.SetField<string>("MotherLastName", person.MotherLastName);
                dataRow.SetField<int?>("AddressId", person.AddressId);
                dataRow.SetField<string>("Street", person.Street);
                dataRow.SetField<int>("OutdoorNumber", person.OutdoorNumber);
                dataRow.SetField<int?>("InteriorNumber", person.InteriorNumber);
                dataRow.SetField<string>("Colony", person.Colony);
                dataRow.SetField<string>("Municipality", person.Municipality);
                dataRow.SetField<int>("StateRepublicId", person.StateRepublicId);
                
                dataTable.Rows.Add(dataRow);

                sqlParameters.Add(new SqlParameter("@p_vtPersonAddress", dataTable));                
                sqlParameters.Add(new SqlParameter("@p_ActionCrudId", (int)EnumCrud.Create));

                intRespondeSP = (int)dataAccessSqlServer.ExecuteScalar("DBQS.sp_PersonAddressCrud", sqlParameters);
                boolResponseSP = Convert.ToBoolean(intRespondeSP);                
            }
            catch (Exception ex)
            {                
                throw;
            }

            return boolResponseSP;
        }

        public ListInfoPersonsAddress GetAllPersonsAddress()
        {
            ListInfoPersonsAddress listInfoPersonsAddress = new ListInfoPersonsAddress();
            DataAccessSqlServer dataAccessSqlServer = new DataAccessSqlServer("AppTestQuetzalConnection");
            List<SqlParameter> sqlParameters = new List<SqlParameter>();
            DataSet dataSet = new DataSet();            
            
            try
            {
                sqlParameters.Add(new SqlParameter("@p_ActionCrudId", (int)EnumCrud.Read));
                dataSet = dataAccessSqlServer.ExecuteDataSet("DBQS.sp_PersonAddressCrud", sqlParameters);

                if (dataSet.Tables != null && dataSet.Tables[0].Rows.Count > 0)
                {
                    listInfoPersonsAddress.PersonsAddress = dataSet.Tables[0].AsEnumerable().Select(m => new Person
                    {
                        PersonId = m.Field<int>("PersonId"),
                        FirstName = m.Field<string>("FirstName"),
                        LastName = m.Field<string>("LastName"),
                        MotherLastName = m.Field<string>("MotherLastName"),
                        AddressId = m.Field<int>("AddressId"),
                        Street = m.Field<string>("Street"),
                        OutdoorNumber = m.Field<int>("OutdoorNumber"),
                        InteriorNumber = m.Field<int?>("InteriorNumber"),
                        Colony = m.Field<string>("Colony"),
                        Municipality = m.Field<string>("Municipality"),
                        StateRepublicId = m.Field<int>("StateRepublicId"),
                    }).ToList();
                }
                else
                {
                    listInfoPersonsAddress.PersonsAddress = new List<Person>();
                }

                if (dataSet.Tables.Count > 1 && dataSet.Tables[1].Rows.Count > 0)
                {
                    listInfoPersonsAddress.StatesRepublic = dataSet.Tables[1].AsEnumerable().Select(m => new StateRepublic
                    {
                        StateRepublicId = m.Field<int>("StateRepublicId"),
                        NameStateRepublic = m.Field<string>("NameStateRepublic"),
                    }).ToList();
                }
                else
                {
                    listInfoPersonsAddress.StatesRepublic = new List<StateRepublic>();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            
            return listInfoPersonsAddress;
        }

        public bool UpdatePersonAddress(Person person)
        {
            DataAccessSqlServer dataAccessSqlServer = new DataAccessSqlServer("AppTestQuetzalConnection");
            List<SqlParameter> sqlParameters = new List<SqlParameter>();
            int intRespondeSP = 0;
            bool boolResponseSP = false;

            try
            {
                DataTable dataTable = new DataTable("tbl_PersonAddressType");

                dataTable.Columns.Add("PersonId", typeof(int));
                dataTable.Columns.Add("FirstName", typeof(string));
                dataTable.Columns.Add("LastName", typeof(string));
                dataTable.Columns.Add("MotherLastName", typeof(string));
                dataTable.Columns.Add("AddressId", typeof(int));
                dataTable.Columns.Add("Street", typeof(string));
                dataTable.Columns.Add("OutdoorNumber", typeof(int));
                dataTable.Columns.Add("InteriorNumber", typeof(int));
                dataTable.Columns.Add("Colony", typeof(string));
                dataTable.Columns.Add("Municipality", typeof(string));
                dataTable.Columns.Add("StateRepublicId", typeof(int));

                DataRow dataRow = dataTable.NewRow();

                dataRow.SetField<int?>("PersonId", person.PersonId);
                dataRow.SetField<string>("FirstName", person.FirstName);
                dataRow.SetField<string>("LastName", person.LastName);
                dataRow.SetField<string>("MotherLastName", person.MotherLastName);
                dataRow.SetField<int?>("AddressId", person.AddressId);
                dataRow.SetField<string>("Street", person.Street);
                dataRow.SetField<int>("OutdoorNumber", person.OutdoorNumber);
                dataRow.SetField<int?>("InteriorNumber", person.InteriorNumber);
                dataRow.SetField<string>("Colony", person.Colony);
                dataRow.SetField<string>("Municipality", person.Municipality);
                dataRow.SetField<int>("StateRepublicId", person.StateRepublicId);

                dataTable.Rows.Add(dataRow);

                sqlParameters.Add(new SqlParameter("@p_vtPersonAddress", dataTable));
                sqlParameters.Add(new SqlParameter("@p_ActionCrudId", (int)EnumCrud.Update));

                intRespondeSP = (int)dataAccessSqlServer.ExecuteScalar("DBQS.sp_PersonAddressCrud", sqlParameters);
                boolResponseSP = Convert.ToBoolean(intRespondeSP);
            }
            catch (Exception ex)
            {
                throw;
            }

            return boolResponseSP;
        }

        public bool DeletePersonAddress(int personId)
        {
            DataAccessSqlServer dataAccessSqlServer = new DataAccessSqlServer("AppTestQuetzalConnection");
            List<SqlParameter> sqlParameters = new List<SqlParameter>();
            int intRespondeSP = 0;
            bool boolResponseSP = false;

            try
            {
                sqlParameters.Add(new SqlParameter("@p_PersonId", personId));
                sqlParameters.Add(new SqlParameter("@p_ActionCrudId", (int)EnumCrud.Delete));

                intRespondeSP = (int)dataAccessSqlServer.ExecuteScalar("DBQS.sp_PersonAddressCrud", sqlParameters);
                boolResponseSP = Convert.ToBoolean(intRespondeSP);
            }
            catch (Exception ex)
            {
                throw;
            }

            return boolResponseSP;
        }
    }
}