﻿namespace AppTestQuetzalDataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.SqlClient;
    using System.Data;

    public class DataAccessSqlServer : IDisposable
    {
        private SqlConnection sqlConnection = null;
        private SqlCommand sqlcommand = null;
        private SqlDataReader sqlDataread = null;
        public string MsgPrint { get; set; }

        public DataAccessSqlServer(string connectionName)
        {
            if (sqlConnection == null)
            {
                sqlConnection = new SqlConnection
                {
                    ConnectionString = ConfigurationManager.ConnectionStrings[connectionName].ConnectionString
                };
                sqlConnection.Open();

                sqlConnection.InfoMessage += sqlConnection_InfoMessage;
            }
            else
            {
                if (sqlConnection.State.Equals(ConnectionState.Open))
                    sqlConnection.Close();

                sqlConnection.ConnectionString = ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
                sqlConnection.Open();
            }
        }

        void sqlConnection_InfoMessage(object sender, SqlInfoMessageEventArgs e)
        {
            MsgPrint = e.Message;
        }

        public void Open()
        {
            if (sqlConnection.State.Equals(ConnectionState.Closed))
                sqlConnection.Open();
        }

        public void Close()
        {
            if (sqlConnection.State.Equals(ConnectionState.Open))
                sqlConnection.Close();
        }
        public DataSet ExecuteDataSet(string nameSp, List<SqlParameter> sqlParameters)
        {
            DataSet ds = new DataSet();
            using (SqlConnection conn = sqlConnection)
            {
                SqlCommand sqlComm = new SqlCommand(nameSp, conn);

                if (sqlParameters != null && sqlParameters.Count > 0)
                {
                    sqlComm.Parameters.AddRange(sqlParameters.ToArray());
                }

                sqlComm.CommandType = CommandType.StoredProcedure;
                sqlComm.CommandTimeout = 3600;

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm;

                da.Fill(ds);
            }
            this.Close();
            return ds;
        }

        public SqlDataReader ExecuteDataRead(string nameSp, List<SqlParameter> sqlParameters)
        {
            sqlcommand = new SqlCommand();
            sqlcommand.Connection = this.sqlConnection;
            sqlcommand.CommandTimeout = 3600;
            sqlcommand.CommandText = nameSp;
            if (sqlParameters != null && sqlParameters.Count > 0)
            {
                sqlcommand.Parameters.AddRange(sqlParameters.ToArray());
            }
            sqlcommand.CommandType = System.Data.CommandType.StoredProcedure;
            sqlDataread = sqlcommand.ExecuteReader();

            return sqlDataread;
        }

        public object ExecuteScalar(string nameSp, List<SqlParameter> sqlParameters)
        {
            object response = null;
            sqlcommand = new SqlCommand();

            try
            {
                sqlcommand.Connection = this.sqlConnection;
                sqlcommand.CommandTimeout = 3600;
                sqlcommand.CommandText = nameSp;
                if (sqlParameters != null && sqlParameters.Count > 0)
                {
                    sqlcommand.Parameters.AddRange(sqlParameters.ToArray());
                }
                sqlcommand.CommandType = System.Data.CommandType.StoredProcedure;
                response = sqlcommand.ExecuteScalar();
                this.Close();
            }
            catch (Exception ex)
            {
                throw;
            }

            return response;
        }

        public void ExecuteScalarOutput(string nameSp, ref List<SqlParameter> sqlParameters)
        {
            sqlcommand = new SqlCommand();
            sqlcommand.Connection = this.sqlConnection;
            sqlcommand.CommandTimeout = 3600;
            sqlcommand.CommandText = nameSp;
            if (sqlParameters != null && sqlParameters.Count > 0)
            {
                sqlcommand.Parameters.AddRange(sqlParameters.ToArray());
            }
            sqlcommand.CommandType = System.Data.CommandType.StoredProcedure;
            sqlcommand.ExecuteScalar();
            this.Close();
        }

        public object ExecuteFunction(string nameFunction, List<SqlParameter> sqlParameters)
        {
            sqlcommand = new SqlCommand();
            sqlcommand.Connection = this.sqlConnection;
            sqlcommand.CommandTimeout = 3600;
            sqlcommand.CommandText = "Select " + nameFunction + "(";
            foreach (SqlParameter p in sqlParameters)
                sqlcommand.CommandText += "@" + p.ParameterName + ",";

            sqlcommand.CommandText = sqlcommand.CommandText.Remove(sqlcommand.CommandText.Length - 1, 1);
            sqlcommand.CommandText += ")";

            if (sqlParameters != null && sqlParameters.Count > 0)
                sqlcommand.Parameters.AddRange(sqlParameters.ToArray());

            sqlcommand.CommandType = CommandType.Text;
            object Respuesta = sqlcommand.ExecuteScalar();
            return Respuesta;
        }

        public object ExecuteNonQuery(string nameSp, List<SqlParameter> sqlParameters)
        {
            sqlcommand = new SqlCommand();
            sqlcommand.Connection = this.sqlConnection;
            sqlcommand.CommandTimeout = 3600;
            sqlcommand.CommandText = nameSp;
            if (sqlParameters != null && sqlParameters.Count > 0)
            {
                sqlcommand.Parameters.AddRange(sqlParameters.ToArray());
            }
            sqlcommand.CommandType = System.Data.CommandType.StoredProcedure;
            object Respuesta = sqlcommand.ExecuteNonQuery();
            this.Close();
            return Respuesta;
        }

        public void Dispose()
        {
            if (sqlConnection.State != ConnectionState.Closed)
                sqlConnection.Close();
            sqlConnection.Dispose();
        }
    }
}
