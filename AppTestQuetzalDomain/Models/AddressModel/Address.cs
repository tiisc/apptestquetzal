﻿namespace AppTestQuetzalDomain.Models.AddressModel
{
    using System.ComponentModel.DataAnnotations;

    public class Address
    {
        [Key]
        public int AddressId { get; set; }
        
        [StringLength(150)]
        [Required]
        public string Street { get; set; }

        [Required]
        public int OutdoorNumber { get; set; }

        public int? InteriorNumber { get; set; }

        [StringLength(150)]
        [Required]
        public string Colony { get; set; }

        [StringLength(150)]
        [Required]
        public string Municipality { get; set; }

        public int StateRepublicId { get; set; }        
    }
}