﻿namespace AppTestQuetzalDomain.Models.AddressModel
{
    using System.ComponentModel.DataAnnotations;

    public class StateRepublic
    {
        [Key]
        public int StateRepublicId { get; set; }

        [StringLength(350)]
        [Required]
        public string NameStateRepublic { get; set; }
    }
}