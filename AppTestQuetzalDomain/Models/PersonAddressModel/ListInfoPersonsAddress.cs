﻿namespace AppTestQuetzalDomain.Models.PersonAddressModel
{
    using System.Collections.Generic;
    using AppTestQuetzalDomain.Models.AddressModel;
    using AppTestQuetzalDomain.Models.PersonModel;

    public class ListInfoPersonsAddress
    {
        public List<Person> PersonsAddress { get; set; }
        public List<StateRepublic> StatesRepublic { get; set; }
    }
}