﻿namespace AppTestQuetzalDomain.Models.PersonModel
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using AppTestQuetzalDomain.Models.AddressModel;

    public class Person : Address
    {
        [Key]
        public int PersonId { get; set; }
        
        [StringLength(150)]
        [Required]
        public string FirstName { get; set; }

        [StringLength(150)]
        [Required]
        public string LastName { get; set; }

        [StringLength(150)]
        [Required]
        public string MotherLastName { get; set; }

        [NotMapped]
        public int ActionCrudId { get; set; }        
    }
}