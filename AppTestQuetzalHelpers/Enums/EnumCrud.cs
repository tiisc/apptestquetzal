﻿namespace AppTestQuetzalHelpers.Enums
{
    public enum EnumCrud
    {
        Create = 1,
        Read = 2,
        Update = 3,
        Delete = 4
    }
}