USE [master]
GO

--DROP DATABASE DBAPPTESTQUETZAL

IF DB_ID('DBAPPTESTQUETZAL') IS NULL
	CREATE DATABASE DBAPPTESTQUETZAL
GO

USE [DBAPPTESTQUETZAL]
GO

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE NAME = 'DBQS')
	EXEC('CREATE SCHEMA DBQS;')
GO

/** TABLE Person ************************************************************************************************************************************************************/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'DBQS' AND TABLE_NAME = 'tbl_Person')
BEGIN
	CREATE TABLE [DBQS].[tbl_Person](
	[PersonId] [int] IDENTITY(1,1) NOT NULL,	
	[FirstName] [varchar](150) NOT NULL,	
	[LastName] [varchar](150) NOT NULL,	
	[MotherLastName] [nvarchar](150) NOT NULL,
	 CONSTRAINT [PK_DBQS.tbl_Person] PRIMARY KEY CLUSTERED 
	(
		[PersonId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
END
GO

/** TABLE StateRepublic ************************************************************************************************************************************************************/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'DBQS' AND TABLE_NAME = 'tbl_CatStateRepublic')
BEGIN
	CREATE TABLE [DBQS].[tbl_CatStateRepublic](
	[StateRepublicId] [int] IDENTITY(1,1) NOT NULL,	
	[NameStateRepublic] [varchar](350) NOT NULL,		
	 CONSTRAINT [PK_DBQS.tbl_CatStateRepublic] PRIMARY KEY CLUSTERED 
	(
		[StateRepublicId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
END
GO

/** TABLE Address ************************************************************************************************************************************************************/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'DBQS' AND TABLE_NAME = 'tbl_Address')
BEGIN
	CREATE TABLE [DBQS].[tbl_Address](
	[AddressId] [int] IDENTITY(1,1) NOT NULL,	
	[Street] [varchar](150) NOT NULL,	
	[OutdoorNumber] [int] NOT NULL,
	[InteriorNumber] [int] NULL,
	[Colony] [varchar](150) NOT NULL,	
	[Municipality] [varchar](150) NOT NULL,
	[StateRepublicId] [int] NOT NULL,
	[PersonId] [int] NOT NULL,
	 CONSTRAINT [PK_DBQS.AddressId] PRIMARY KEY CLUSTERED 
	(
		[AddressId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [DBQS].[tbl_Address]  WITH CHECK ADD  CONSTRAINT [FK_Address_REF_Person] FOREIGN KEY([PersonId])
	REFERENCES [DBQS].[tbl_Person] ([PersonId])	
	ALTER TABLE [DBQS].[tbl_Address] CHECK CONSTRAINT [FK_Address_REF_Person]

	ALTER TABLE [DBQS].[tbl_Address]  WITH CHECK ADD  CONSTRAINT [FK_Address_REF_StateRepublic] FOREIGN KEY([StateRepublicId])
	REFERENCES [DBQS].[tbl_CatStateRepublic] ([StateRepublicId])	
	ALTER TABLE [DBQS].[tbl_Address] CHECK CONSTRAINT [FK_Address_REF_StateRepublic]
END
GO

/** INSERT VALUES TABLE tbl_CatStateRepublic ************************************************************************************************************************************************************/
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'DBQS' AND TABLE_NAME = 'tbl_CatStateRepublic')
BEGIN
	INSERT INTO [DBQS].[tbl_CatStateRepublic] ([NameStateRepublic]) VALUES ('Aguascalientes')
	INSERT INTO [DBQS].[tbl_CatStateRepublic] ([NameStateRepublic]) VALUES ('Baja California')
	INSERT INTO [DBQS].[tbl_CatStateRepublic] ([NameStateRepublic]) VALUES ('Baja California Sur')
	INSERT INTO [DBQS].[tbl_CatStateRepublic] ([NameStateRepublic]) VALUES ('Chiapas')
	INSERT INTO [DBQS].[tbl_CatStateRepublic] ([NameStateRepublic]) VALUES ('Chihuahua')
	INSERT INTO [DBQS].[tbl_CatStateRepublic] ([NameStateRepublic]) VALUES ('Chihuahua')
	INSERT INTO [DBQS].[tbl_CatStateRepublic] ([NameStateRepublic]) VALUES ('Estado de M�xico')
	INSERT INTO [DBQS].[tbl_CatStateRepublic] ([NameStateRepublic]) VALUES ('Puebla')
	INSERT INTO [DBQS].[tbl_CatStateRepublic] ([NameStateRepublic]) VALUES ('Ciudad de M�xico')
END
GO

/** CREATE TABLE TYPE PersonAddress  *******************************************************************************************************************************************************/
IF EXISTS (SELECT * FROM SYS.table_types WHERE user_type_id = Type_id(N'DBQS.tbl_PersonAddressType'))
	DROP TYPE [DBQS].[tbl_tbl_PersonAddressType]
GO

IF NOT EXISTS(SELECT * FROM SYS.table_types WHERE user_type_id = Type_id(N'DBQS.tbl_PersonAddressType'))
BEGIN
	CREATE TYPE [DBQS].[tbl_PersonAddressType] AS TABLE(
		[PersonId] [int] NULL,	
		[FirstName] [varchar](150) NOT NULL,	
		[LastName] [varchar](150) NOT NULL,	
		[MotherLastName] [nvarchar](150) NOT NULL,
		[AddressId] [int] NULL,	
		[Street] [varchar](150) NOT NULL,	
		[OutdoorNumber] [int] NOT NULL,
		[InteriorNumber] [int] NULL,
		[Colony] [varchar](150) NOT NULL,	
		[Municipality] [varchar](150) NOT NULL,
		[StateRepublicId] [int] NOT NULL		
	)
END
GO

/** SP CRUD  *******************************************************************************************************************************************************/
IF EXISTS (SELECT * FROM SYS.procedures WHERE object_id = OBJECT_ID(N'DBQS.sp_PersonAddressCrud'))
	DROP PROCEDURE [DBQS].[sp_PersonAddressCrud]
GO

/** CREATE SP DBQS.sp_PersonAddressCrud *************************************************************************************************************************************************************************************/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	/*******************************************************************************************************************************************
	-- Author: Hern�ndez D�az Oscar
	-- Description: Perform all CRUD operations for the objects people with their address
	-- Creation Date: 2019-04-02			
	********************************************************************************************************************************************/
	CREATE PROCEDURE [DBQS].[sp_PersonAddressCrud]
	(		
		@p_ActionCrudId							INT,
		@p_vtPersonAddress						AS DBQS.tbl_PersonAddressType READONLY,
		@p_PersonId								INT = NULL		
	)
	AS
	BEGIN
		SET NOCOUNT ON;

		DECLARE @vLastPersonId TABLE(PersonId INT);

		DECLARE 
				@Create INT,
				@Read	INT,
				@Update INT,
				@Delete INT

				
		
		SET @Create =	1
		SET @Read	=	2
		SET @Update =	3
		SET @Delete =	4
				
		IF(@p_ActionCrudId = @Create)
		BEGIN			
			BEGIN TRY
				BEGIN TRANSACTION InsertPersonAddress

					INSERT INTO DBQS.tbl_Person(FirstName, LastName, MotherLastName) OUTPUT inserted.PersonId INTO @vLastPersonId(PersonId)
					SELECT VTP.FirstName, VTP.LastName, VTP.MotherLastName
					FROM @p_vtPersonAddress VTP
					
					INSERT INTO DBQS.tbl_Address(Street, OutdoorNumber, InteriorNumber, Colony, Municipality, StateRepublicId, PersonId)
					SELECT VTPA.Street, VTPA.OutdoorNumber, VTPA.InteriorNumber, VTPA.Colony, VTPA.Municipality, VTPA.StateRepublicId, (SELECT PersonId FROM @vLastPersonId) AS PersonId
					FROM @p_vtPersonAddress VTPA

					SELECT 1 AS ResponseSP

				COMMIT TRANSACTION InsertPersonAddress
			END TRY
			BEGIN CATCH
				IF (@@TRANCOUNT > 0)
					ROLLBACK TRANSACTION
				SELECT 0 AS ResponseSP
			END CATCH
		END

		IF(@p_ActionCrudId = @Read)
		BEGIN
			SELECT	TP.PersonId,
					TP.FirstName,
					TP.LastName,
					TP.MotherLastName,
					TA.AddressId,					
					TA.Street,					
					TA.OutdoorNumber,
					TA.InteriorNumber,
					TA.Colony,
					TA.Municipality,
					TA.StateRepublicId					
			FROM DBQS.tbl_Person TP WITH(NOLOCK)
				LEFT JOIN DBQS.tbl_Address TA WITH(NOLOCK)
					ON TP.PersonId = TA.PersonId								
								
			SELECT	TCSR.StateRepublicId,
					TCSR.NameStateRepublic
			FROM DBQS.tbl_CatStateRepublic TCSR WITH(NOLOCK)
		END

		
		IF(@p_ActionCrudId = @Update)
		BEGIN			
			BEGIN TRY
				BEGIN TRANSACTION UpdatePersonAddress
									
					UPDATE	TP
					SET		TP.FirstName = VTP.FirstName,
							TP.LastName = VTP.LastName,
							TP.MotherLastName = VTP.MotherLastName
					FROM DBQS.tbl_Person TP
						INNER JOIN @p_vtPersonAddress VTP
							ON TP.PersonId = VTP.PersonId

					UPDATE	TA
					SET		TA.Street = VTPA.Street,
							TA.InteriorNumber = VTPA.InteriorNumber,
							TA.OutdoorNumber = VTPA.OutdoorNumber,
							TA.Colony = VTPA.Colony,
							TA.Municipality = VTPA.Municipality,
							TA.StateRepublicId = VTPA.StateRepublicId
					FROM DBQS.tbl_Address TA
						INNER JOIN @p_vtPersonAddress VTPA
							ON TA.AddressId = VTPA.AddressId

					SELECT 1 AS ResponseSP

				COMMIT TRANSACTION UpdatePersonAddress
			END TRY
			BEGIN CATCH
				IF (@@TRANCOUNT > 0)
					ROLLBACK TRANSACTION
				SELECT 0 AS ResponseSP
			END CATCH
		END

		IF(@p_ActionCrudId = @Delete)
		BEGIN			
			BEGIN TRY
				BEGIN TRANSACTION DeletePersonAddress

					DELETE FROM DBQS.tbl_Address WHERE PersonId = @p_PersonId
					DELETE FROM DBQS.tbl_Person WHERE PersonId = @p_PersonId
															
					SELECT 1 AS ResponseSP

				COMMIT TRANSACTION DeletePersonAddress
			END TRY
			BEGIN CATCH
				IF (@@TRANCOUNT > 0)
					ROLLBACK TRANSACTION
				SELECT 0 AS ResponseSP
			END CATCH
		END
END
GO